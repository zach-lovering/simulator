function speedAverage=missionProfile(speedCruise,distance,varargin)

ip=inputParser;
addRequired( ip, 'speedCruise',             @isnumeric); %Vehicle cruise speed [s]
addRequired( ip, 'distance',                @isnumeric); %Trip distance [s]
addParameter(ip, 'accel',           1.7,    @isnumeric); %Average acceleration rate during transition
addParameter(ip, 'decel',           1.3,    @isnumeric); %Average deceleration rate during transition
addParameter(ip, 'timeTakeoff',     100,    @isnumeric); %Average time to take off (hover+translate+up)
addParameter(ip, 'timeLand',        100,    @isnumeric); %Average time to land (down+translate+hover)
parse(ip,speedCruise,distance,varargin{:})
shatter(ip.Results);

timeAccelNoCruise=sqrt(2*distance/accel/(1+accel/decel)); %Time to accelerate to max speed (if cruise not reached)

timeAccel=speedCruise/accel; %Time to accelerate to cruise speed
if timeAccelNoCruise>timeAccel %If cruise speed is reached
    timeDecel=speedCruise/decel; 
    timeCruise=(distance-0.5*(accel*timeAccel^2+decel*timeDecel^2))/speedCruise;
else
    timeAccel=timeAccelNoCruise;
    timeDecel=accel*timeAccel/decel;
    timeCruise=0;
end
timeFly=timeTakeoff+timeAccel+timeDecel+timeCruise+timeLand; %Time to fly mission
speedAverage=distance/timeFly; %Average trip speed