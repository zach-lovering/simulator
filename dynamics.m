function [pax,vehicle]=dynamics(pax,vehicle,t,dt,distance)


%% Vehicle state
%0 Morning queue
%1 At helipad 1
%2 Flying towards helipad 2
%3 Waiting in hover at helipad 2
%4 At helipad 2
%5 Flying towards helipad 1
%6 Waiting in hover at helipad 1

for j=1:length(vehicle)
    i=length(vehicle{j}.time);
    vehicle{j}.time(i+1)=vehicle{j}.time(i)+dt;
    switch vehicle{j}.state(end)
        case 0 %Morning queue
            vehicle{j}.position(i+1)=0;
        case 1 %At helipad 1
            vehicle{j}.position(i+1)=0;
            vehicle{j}.timer=vehicle{j}.timer+dt; %charging/preflight timer
        case 2 %Flying towards helipad 2
            speedAverage=missionProfile(vehicle{j}.maxCruiseSpeed,distance);
            vehicle{j}.position(i+1)=vehicle{j}.position(i)+dt*speedAverage;
            if vehicle{j}.position(i+1)>distance; vehicle{j}.position(i+1)=distance; end
        case 3 %Waiting in hover at helipad 2
            vehicle{j}.position(i+1)=distance;
        case 4 %At helipad 2
            vehicle{j}.position(i+1)=distance;
            vehicle{j}.timer=vehicle{j}.timer+dt; %charging/preflight timer
        case 5 %Flying towards helipad 1
            speedAverage=missionProfile(vehicle{j}.maxCruiseSpeed,distance);
            vehicle{j}.position(i+1)=vehicle{j}.position(i)-dt*speedAverage;
        case 6 %Waiting in hover at helipad 1
            vehicle{j}.position(i+1)=0;
        otherwise
            warning('Unknown passenger state')
    end
end

%% Passenger travel state
% 0 Inbound to helipad
% 1 Checking in
% 2 Waiting for vehicle to arrive
% 3 Boarding
% 4 Waiting for passengers to load
% 5 Flying
% 6 Alighting
% 7 Outbound to destination
% 8 Finished

for j=1:length(pax)
    i=length(pax{j}.time);
    pax{j}.time(i+1)=pax{j}.time(i)+dt;
    switch pax{j}.state(end)
        case 0 %Inbound to helipad
            pax{j}.position(i+1)=pax{j}.position(i)+dt*pax{j}.speedInbound;
        case 1 %Checking in
            pax{j}.position(i+1)=0;
            pax{j}.timer=pax{j}.timer+dt;
        case 2 %Waiting for vehicle to arrive
            pax{j}.position(i+1)=0;
        case 3 %Boarding
            pax{j}.position(i+1)=0;
            pax{j}.timer=pax{j}.timer+dt;
        case 4 %Waiting for passengers to load
            pax{j}.position(i+1)=0;
        case 5 %Flying
            k=pax{j}.vehicle;
            t=find(vehicle{k}.time==pax{j}.time(i+1),1);
            pax{j}.position(i+1)=vehicle{k}.position(t);
            1;
        case 6 %Alighting
            pax{j}.position(i+1)=distance;
            pax{j}.timer=pax{j}.timer+dt;
        case 7 %Outbound to destination
            pax{j}.position(i+1)=pax{j}.position(i)+dt*pax{j}.speedOutbound;
        case 8 %Finished
            pax{j}.position(i+1)=pax{j}.position(i);
        otherwise
            warning('Unknown passenger state')
    end
end