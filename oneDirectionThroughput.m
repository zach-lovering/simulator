clear;clc

monte=300; %Number of monte carlo iterations
pax=1:4; %Number of passengers vehicle can carry
speedCruiseMax=30:20:90; %Maximum cruising speed [m/s]
distance=[10:10:90]*1e3; %Route distance [m]

figure(1); clf
for ipax=1:length(pax)
    avgSpeed=zeros(length(distance),length(speedCruiseMax)); %Initialize variable
    for ispeed=1:length(speedCruiseMax)
        for idist=1:length(distance)
            speedCruise=missionProfile(speedCruiseMax(ispeed),distance(idist)); %Get average flight speed
            effectiveSpeed=zeros(monte,pax(ipax)); %Initialize variable
            for imonte=1:monte
                effectiveSpeed(imonte,:)=oneDirectionSimulator(speedCruiseMax(ispeed),distance(idist),pax(ipax));
            end
            avgSpeed(idist,ispeed)=mean(mean(effectiveSpeed));
        end
    end
    
    %% Plot results
    subplot(2,2,ipax); hold on
    for ispeed=1:length(speedCruiseMax)
        plot(distance/1000,avgSpeed(:,ispeed));
        legendName{ispeed}=[num2str(speedCruiseMax(ispeed)) ' m/s'];
    end
    xlabel('Distance [km]')
    ylabel('Average Trip Speed [m/s]')
    legend(legendName{:},'location','southeast')
    title([num2str(pax(ipax)) ' Passenger'])
end