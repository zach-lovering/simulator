
%% Initialize
clear;clc
time=[0:60:1*3600]; %Time [s]
dt=time(2)-time(1); %Timestep
distance=40e3; %Distance between helipads [m]

pax=createPax(0); %Initialize pax data
vehicle=createVehicle(7); %Initialize vehicle data
for i=1:length(time)
    requests=requestModel(time(i),dt); %Get # customer requests
    pax=createPax(requests,'time',time(i),'pax',pax); %Create pax
    [pax,vehicle]=dynamics(pax,vehicle,time(i),dt,distance); %Update pax and vehicle positions and timers
    if i<length(time)
        [pax,vehicle]=updateState(pax,vehicle,distance,dt); %Update pax and vehicle positions and timers
    end
end
%%
figure(1); clf

timeScale=3600;
for i=1:length(vehicle)
    subplot(3,1,2); hold on
    plot(vehicle{i}.time/timeScale,vehicle{i}.position/1000,'.');
    ylabel('Vehicle Position [km]')
    
    subplot(3,1,1); hold on
    plot(vehicle{i}.time/timeScale,vehicle{i}.state);
end
yticks(0:8)
yticklabels({'Queue','At pad 1','Fly to 2','Hover at 2',...
    'At pad 2','Fly to 1','Hover at 1'})


for i=1:length(pax)
    subplot(3,1,2); hold on
    plot(pax{i}.time/timeScale,pax{i}.position/1000);
    ylabel('Passenger Position [km]')
    
    subplot(3,1,3); hold on
    plot(pax{i}.time/timeScale,pax{i}.state);
end
yticks(0:8)
yticklabels({'Inbound','Checkin','Waiting for vehicle','Boarding',...
    'Waiting for takeoff','Flying','Alighting','Outbound','Finished'})

for i=1:3
    subplot(3,1,i)
    xlimits=xlim;
    switch timeScale
        case 1
            xticks(0:1:xlimits(2));
            xlabel('Time [s]');
        case 60
            xticks(0:1:xlimits(2));
            xlabel('Time [min]');
        case 3600
            xticks(0:1:xlimits(2));
            xlabel('Time [hr]');
        case 86400
            xticks(0:1:xlimits(2));
            xlabel('Time [day]');
    end
end

    