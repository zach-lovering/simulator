%Initializes a passenger in the pax structure

function pax=createPax(n,varargin)

ip=inputParser;
addRequired( ip, 'n',                       @isnumeric); %Number of passengers to create
addParameter(ip, 'time',           0,       @isnumeric); %Timestamp at beginning of request
addParameter(ip, 'pax',            cell(0), @iscell);    %Passenger data
addParameter(ip, 'radiusInbound',  10e3,    @isnumeric); %Maximum pax distance from departure helipad [m]
addParameter(ip, 'radiusOutbound', 10e3,    @isnumeric); %Maximum destination distance from arrival helipad [m]
addParameter(ip, 'speedInbound',   10,      @isnumeric); %Pax speed getting to the departure helipad [m/s]
addParameter(ip, 'speedOutbound',  10,      @isnumeric); %Pax speed leaving the arrival helipad [m/s]
addParameter(ip, 'timeCheckin',    120,     @isnumeric); %Time required to checkin [s]
addParameter(ip, 'timeBoard',      120,     @isnumeric); %Time required to board [s]
addParameter(ip, 'timeAlight',     120,     @isnumeric); %Time required to alight [s]
parse(ip,n,varargin{:})
shatter(ip.Results);

% Update passenger data
for i=1:n
    n=length(pax)+1;
    pax{n,1}.startRadius=radiusInbound*rand; %Pax starting locations (distance to helipad)
    pax{n,1}.startAngle=2*pi*rand; %Vector angle to starting point from departure helipad
    pax{n,1}.finishRadius=radiusOutbound*rand; %Pax endinig locations (distance from helipad)
    pax{n,1}.finishAngle=2*pi*rand; %Vector angle to ending point from arrival helipad
    pax{n,1}.speedInbound=speedInbound;
    pax{n,1}.speedOutbound=speedOutbound;
    pax{n,1}.state=0; %Pax state
    pax{n,1}.time=time; %Timestamp at start
    pax{n,1}.timer=0; %Timer
    pax{n,1}.position=-pax{n,1}.startRadius; %Pax position (referenced to helipad 1)
    pax{n,1}.vehicle=0; %ID of vehicle (0 means none yet selected)
    pax{n,1}.timeCheckin=timeCheckin;
    pax{n,1}.timeBoard=timeBoard;
    pax{n,1}.timeAlight=timeAlight;
end