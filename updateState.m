function [pax,vehicle]=updateState(pax,vehicle,distance,dt)

%% Vehicle state
%0 Morning queue
%1 At helipad 1
%2 Flying towards helipad 2
%3 Waiting in hover at helipad 2
%4 At helipad 2
%5 Flying towards helipad 1
%6 Waiting in hover at helipad 1

%Get latest vehicle states
for j=1:length(vehicle)
    vehicleState(j)=vehicle{j}.state(end);
end

for j=1:length(vehicle)
    i=length(vehicle{j}.time);
    switch vehicle{j}.state(end)
        case 0 %Morning queue
            if ~any(vehicleState==1) %If helipad 1 is unoccupied
                vehicle{j}.state(i+1)=1; %CHANGE LOGIC HERE TO NOT FIRST BY ID, BUT BY "FIRST COME FIRST SERVE" USING TIMER
                vehicle{j}.timer=dt;
            else
                vehicle{j}.state(i+1)=0;
            end
            
        case 1 %At helipad 1
            if vehicle{j}.timer<vehicle{j}.chargeTime
                vehicle{j}.timer=vehicle{j}.timer+dt;
                vehicle{j}.state(i+1)=1;
            else
                vehicle{j}.timer=0;
                if length(vehicle{j}.load)==vehicle{j}.pax %Check that vehicle is full
                    for k=1:length(vehicle{j}.load)
                        paxState(k)=pax{vehicle{j}.load(k)}.state(end);
                    end
                    if all(paxState==4)
                        vehicle{j}.state(i+1)=2;
                    else
                        vehicle{j}.state(i+1)=1;
                    end
                else
                    vehicle{j}.state(i+1)=1;
                end
            end
            
        case 2 %Flying towards helipad 2
            if vehicle{j}.position(i)>=distance
                vehicle{j}.position(i)=distance;
                if ~any(vehicleState==4) %If helipad 2 is unoccupied
                    vehicle{j}.state(i+1)=4; %CHANGE LOGIC HERE TO NOT FIRST BY ID, BUT BY "FIRST COME FIRST SERVE" USING TIMER
                else
                    vehicle{j}.state(i+1)=3;
                end
            else
                vehicle{j}.state(i+1)=2;
            end
            
        case 3 %Waiting in hover at helipad 2
            if ~any(vehicleState==4) %If helipad 2 is unoccupied
                vehicle{j}.state(i+1)=4; %CHANGE LOGIC HERE TO NOT FIRST BY ID, BUT BY "FIRST COME FIRST SERVE"
                vehicle{j}.timer=dt;
            else
                vehicle{j}.state(i+1)=3;
            end
            
        case 4 %At helipad 2
            if vehicle{j}.timer<vehicle{j}.chargeTime
                vehicle{j}.timer=vehicle{j}.timer+dt;
                vehicle{j}.state(i+1)=4;
            else
                vehicle{j}.timer=0;
                if isempty(vehicle{j}.load)
                    vehicle{j}.state(i+1)=5;
                else
                    vehicle{j}.state(i+1)=4;
                end
            end
            
        case 5 %Flying towards helipad 1
            if vehicle{j}.position(i)<=0
                vehicle{j}.position(i)=0;
                if ~any(vehicleState==1) %If helipad 1 is unoccupied
                    vehicle{j}.state(i+1)=1; %CHANGE LOGIC HERE TO NOT FIRST BY ID, BUT BY "FIRST COME FIRST SERVE" USING TIMER
                else
                    vehicle{j}.state(i+1)=6;
                end
            else
                vehicle{j}.state(i+1)=5;
            end
            
        case 6 %Waiting in hover at helipad 1
            if ~any(vehicleState==1) %If helipad 1 is unoccupied
                vehicle{j}.state(i+1)=1; %CHANGE LOGIC HERE TO NOT FIRST BY ID, BUT BY "FIRST COME FIRST SERVE"
            else
                vehicle{j}.state(i+1)=6;
            end
            
        otherwise
            warning('Unknown passenger state')
    end
    vehicleState(j)=vehicle{j}.state(i+1);
end

%% Passenger travel state
% 0 Inbound to helipad
% 1 Checking in
% 2 Waiting for vehicle to arrive
% 3 Boarding
% 4 Waiting for passengers to load
% 5 Flying
% 6 Alighting
% 7 Outbound to destination
% 8 Finished

for j=1:length(pax)
    i=length(pax{j}.time);
    switch pax{j}.state(end)
        case 0 %Inbound to helipad
            if pax{j}.position(i)>=0
                pax{j}.state(i+1)=1;
                pax{j}.timer=dt;
                pax{j}.position(i)=0;
            else
                pax{j}.state(i+1)=0;
            end
            
        case 1 %Checking in
            if pax{j}.timer<pax{j}.timeCheckin
                pax{j}.timer=pax{j}.timer+dt;
                pax{j}.state(i+1)=1;
            else
                pax{j}.timer=0;
                k=find(vehicleState==1,1,'first');
                if ~isempty(k)
                    if length(vehicle{k}.load)<vehicle{k}.pax
                        pax{j}.state(i+1)=3;
                        pax{j}.vehicle=k;
                        vehicle{k}.load=[vehicle{k}.load j];
                    else
                        pax{j}.state(i+1)=2;
                    end
                else
                    pax{j}.state(i+1)=2;
                end
            end
            
        case 2 %Waiting for vehicle to arrive
            k=find(vehicleState==1,1,'first');
            if ~isempty(k)
                if length(vehicle{k}.load)<vehicle{k}.pax
                    pax{j}.state(i+1)=3;
                    pax{j}.vehicle=k;
                    vehicle{k}.load=[vehicle{k}.load j];
                    pax{j}.timer=dt;
                else
                    pax{j}.state(i+1)=2;
                end
            else
                pax{j}.state(i+1)=2;
            end
            
        case 3 %Boarding
            if pax{j}.timer<pax{j}.timeBoard
                pax{j}.timer=pax{j}.timer+dt;
                pax{j}.state(i+1)=3;
            else
                k=pax{j}.vehicle;
                pax{j}.timer=0;
                if vehicle{k}.state(end)==2
                    pax{j}.state(i+1)=5;
                else
                    pax{j}.state(i+1)=4;
                end
            end
            
        case 4 %Waiting for vehicle to be ready
            k=pax{j}.vehicle;
            if vehicle{k}.state(end)==2
                pax{j}.state(i+1)=5;
            else
                pax{j}.state(i+1)=4;
            end
            
        case 5 %Flying
            k=pax{j}.vehicle;
            if vehicle{k}.state(end)==4
                pax{j}.state(i+1)=6;
                pax{j}.timer=dt;
            else
                pax{j}.state(i+1)=5;
            end
            
        case 6 %Alighting
            if pax{j}.timer<pax{j}.timeAlight
                pax{j}.timer=pax{j}.timer+dt;
                pax{j}.state(i+1)=6;
            else
                k=pax{j}.vehicle;
                pax{j}.timer=0;
                vehicle{k}.load(vehicle{k}.load==j)=[];
                pax{j}.state(i+1)=7;
            end
            
        case 7 %Outbound to destination
            if pax{j}.position(i)>=distance+pax{j}.finishRadius
                pax{j}.state(i+1)=8;
            else
                pax{j}.state(i+1)=7;
            end
            
        case 8 %Finished
            if pax{j}.state(i)==8
                pax{j}.time(i)=[]; %Stop recording movement once trip is finished
            else
                pax{j}.state(i+1)=8;
            end
            
        otherwise
            warning('Unknown passenger state')
            
    end
end