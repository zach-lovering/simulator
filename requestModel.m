%A basic two-hump model is used to generate user demand as a function of
%time of day.

function requests=requestModel(time,dt,varargin)

ip=inputParser;
addRequired( ip, 'time',                     @isnumeric); %Current time of day [s]
addRequired( ip, 'dt',                       @isnumeric); %Amount of passed time in sample [s]
addParameter(ip, 'maxDemand',       0.003,   @isnumeric); %Average pax/s during peak
addParameter(ip, 'minDemand',       0.001,   @isnumeric); %Average pax/s during offpeak
addParameter(ip, 'stddev',          0,       @isnumeric); %Standard deviation on pax/s

parse(ip,time,dt,varargin{:})
shatter(ip.Results);

%% Demand model

%Morning rush
start=6*3600; %Morning peak starts at 6am
width=2*3600; %Morning peak lasts 2 hours
if time>start && time<(start+width)
    demand=minDemand+0.5*(maxDemand-minDemand)*(1-cos(2*pi*(time-start)/width));
else
    demand=minDemand;
end

%Evening rush
start=17*3600; %Evening peak starts at 5pm
width=2*3600;  %Evening peak lasts 2 hours
if time>start && time<(start+width)
    demand=minDemand+0.5*(maxDemand-minDemand)*(1-cos(2*pi*(time-start)/width));
else
    demand=minDemand;
end

%Number of requests to generate
demand=demand*dt; %Scale request count by time passed
requests=floor(demand+randn*stddev);
if rand<(demand-floor(demand))
    requests=requests+1;
end