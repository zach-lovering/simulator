#README
Run `singleRouteSimulator.m` to execute simulation.

##animateTraffic.m
Animates vehicle and passenger movement.

##createPax.m
Creates the passenger data structure and adds passengers to the database at the user's request.

##createVehicle.m
Creates the vehile data structure and adds vehicles to the database at the user's request.

##dynamics.m
Executes on the passenger and vehicle dynamics. Increments the passenger and vehicle position. Also manages passenger and vehicle clocks and timers.

##missionProfile.m
For the provided vehicle performance specifications, provides the average vehicle speed for a trip of a given length.

##oneDirectionSimulator.m
Simple model to estimate the total trip effective speed, including passengers getting to/from the landing zones, waiting for other passengers to arrive, checkin time, and boarding time.

##oneDirectionThroughput.m
Runs a monte carlo using oneDirectionSimulator.m to see average results for various user start/end points.

##requestModel.m
Provides the number of passenger requests over a given time period. It is based on a simple two-humped demand model (morning and evening rushes).

##singleRouteSimulator.m
Runs the simulation for a single route. Will also generate time history plots.

##updateState.m
Updates the vehicle and passegner states based on current timers, vehicle/passenger status, and position.