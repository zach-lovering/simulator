function x=shatter(x)
% SHATTER assigns each field in the input structure to its own variable in
% the caller workspace. If an output is requested, fields in the input
% structure will be updated with the values in workspace that match each
% field's name.

vars=fieldnames(x);
for i=1:length(vars)
    if nargout==0
        assignin('caller',vars{i},x.(vars{i}));
    else
        x=setfield(x,vars{i},evalin('caller',vars{i}));
    end
end