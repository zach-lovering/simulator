%Initializes a passenger in the pax structure

function vehicle=createVehicle(n,varargin)

ip=inputParser;
addRequired( ip, 'n',                        @isnumeric); %Number of vehicles to create
addParameter(ip, 'time',            0,       @isnumeric); %Timestamp at beginning of request
addParameter(ip, 'vehicle',         cell(0), @iscell);    %Vehicle data
addParameter(ip, 'pax',             1,       @isnumeric); %Pax per vehicle
addParameter(ip, 'maxCruiseSpeed',  50,      @isnumeric); %Maximum vehicle cruise speed [m/s]
addParameter(ip, 'chargeTime',      0,       @isnumeric); %Time to charge vehicle [s]
parse(ip,n,varargin{:})
shatter(ip.Results);

% Update passenger data
for i=1:n
    n=length(vehicle)+1;
    vehicle{n,1}.state=0; %Vehicle state
    vehicle{n,1}.load=[]; %Number of pax on board
    vehicle{n,1}.time=time; %Timestamp
    vehicle{n,1}.timer=0; %Timer
    vehicle{n,1}.maxCruiseSpeed=maxCruiseSpeed; %Timer
    vehicle{n,1}.position=0; %Pax position (referenced to helipad 1)
    vehicle{n,1}.chargeTime=0; %Pax position (referenced to helipad 1)
    vehicle{n,1}.pax=pax; %Number of seats on vehicle
end