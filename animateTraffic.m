%function animateTraffic(pax,vehicle,time)
sv=50; sp=20;

h = figure;
axis tight manual % this ensures that getframe() returns a consistent size
filename = 'testAnimated.gif';

for i=1:length(time)
    plot(0,0);
    hold on
    for j=1:length(vehicle)
        k=find(vehicle{j}.time==time(i),1,'first');
        if ~isempty(k)
            switch vehicle{j}.state(k)
                case 0 %Morning queue
                    plot(vehicle{j}.position(k)/1000,-1,'r.','markersize',sv)
                    
                case 1 %At helipad 1
                    plot(vehicle{j}.position(k)/1000,0,'r.','markersize',sv)
                    
                case 2 %Flying towards helipad 2
                    plot(vehicle{j}.position(k)/1000,2,'r.','markersize',sv)
                    
                case 3 %Waiting in hover at helipad 2
                    plot(vehicle{j}.position(k)/1000,1,'r.','markersize',sv)
                    
                case 4 %At helipad 2
                    plot(vehicle{j}.position(k)/1000,0,'r.','markersize',sv)
                    
                case 5 %Flying towards helipad 1
                    plot(vehicle{j}.position(k)/1000,2,'r.','markersize',sv)
                    
                case 6 %Waiting in hover at helipad 1
                    plot(vehicle{j}.position(k)/1000,1,'r.','markersize',sv)
                    
                otherwise
                    warning('Unknown passenger state')
            end
            xlim([-10 80]);
            ylim([-4 3]);
        end
    end
    
    for j=1:length(pax)
        k=find(pax{j}.time==time(i),1,'first');
        if ~isempty(k)
            switch pax{j}.state(k)
                case 0 %Inbound to helipad
                    plot(pax{j}.position(k)/1000,-3,'b.','markersize',sp)
                    
                case 1 %Checking in
                    plot(pax{j}.position(k)/1000,-2,'b.','markersize',sp)
                    
                case 2 %Waiting for vehicle to arrive
                    plot(pax{j}.position(k)/1000,-1,'b.','markersize',sp)
                    
                case 3 %Boarding
                    plot(pax{j}.position(k)/1000,-1,'b.','markersize',sp)
                    
                case 4 %Waiting for passengers to load
                    plot(pax{j}.position(k)/1000,0,'b.','markersize',sp)
                    
                case 5 %Flying
                    plot(pax{j}.position(k)/1000,2,'b.','markersize',sp)
                    
                case 6 %Alighting
                    plot(pax{j}.position(k)/1000,0,'b.','markersize',sp)
                    
                case 7 %Outbound to destination
                    plot(pax{j}.position(k)/1000,-3,'b.','markersize',sp)
                    
                case 8 %Finished
                    
                otherwise
                    warning('Unknown passenger state')
            end
            xlim([-10 80]);
            ylim([-4 3]);
        end
    end
    hold off
    drawnow
    
    % Capture the plot as an image
    frame = getframe(h);
    im = frame2im(frame);
    [imind,cm] = rgb2ind(im,256);
    % Write to the GIF File
    if i == 1
        imwrite(imind,cm,filename,'gif','DelayTime',0.01, 'Loopcount',inf);
    else
        imwrite(imind,cm,filename,'gif','DelayTime',0.01,'WriteMode','append');
    end
end
