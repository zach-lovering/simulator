function effectiveSpeed=oneDirectionSimulator(speedCruiseMax,distance,pax,varargin)

ip=inputParser;
addRequired(ip, 'speedCruiseMax',   @isnumeric); %Maximum cruising speed [m/s]
addRequired(ip, 'distance',         @isnumeric); %Route distance [m]
addRequired(ip, 'pax',              @isnumeric); %Number of passengers vehicle can carry

addParameter(ip, 'speedInbound',    8,     @isnumeric); %Pax speed getting to the departure helipad [m/s]
addParameter(ip, 'speedOutbound',   8,     @isnumeric); %Pax speed leaving the arrival helipad [m/s]
addParameter(ip, 'radiusInbound',   5e3,   @isnumeric); %Maximum pax distance from departure helipad [m]
addParameter(ip, 'radiusOutbound',  5e3,   @isnumeric); %Maximum destination distance from arrival helipad [m]
addParameter(ip, 'timeCheckin',     120,    @isnumeric); %Time to perform checkin, security, etc
addParameter(ip, 'timeBoard',       120,    @isnumeric); %Time for a single pax to board
addParameter(ip, 'timeAlight',      120,    @isnumeric); %Time for a single pax to alight
addParameter(ip, 'drawon',          false,  @islogical); %Time for a single pax to alight

parse(ip,speedCruiseMax,distance,pax,varargin{:})
shatter(ip.Results);

%% Initialize
time=[0:1:100]*60; %Time [s]
dt=time(2)-time(1); %Timestep
paxD=zeros(length(time),pax); %Initialize pax positions
waitWatch=zeros(1,pax);

start=-radiusInbound*rand(1,pax); %Pax starting locations (distance to helipad)
startAngle=2*pi*rand(1,pax); %Vector angle to starting point from departure helipad
finish=radiusOutbound*rand(1,pax); %Pax endinig locations (distance from helipad)
finishAngle=2*pi*rand(1,pax); %Vector angle to ending point from arrival helipad

%Vehicle state
flyState=-2;
%-3 morning queue
%-2 at helipad 1
%-1 flying towards helipad 1
% 0 waiting in hover
% 1 flying towards helipad 2
% 2 at helipad 2

%Passenger travel state
paxState=zeros(length(time),pax);
% 0 Inbound to helipad
% 1 Checking in
% 2 Waiting for vehicle to arrive
% 3 Boarding
% 4 Waiting for passengers to load
% 5 Flying
% 6 Alighting
% 7 Outbound to destination
% 8 Finished

paxD(1,:)=start;

speedCruise=missionProfile(speedCruiseMax,distance); %Get average flight speed
for i=1:length(time)-1
    paxState(i+1,:)=paxState(i,:);
    for j=1:pax
        switch paxState(i,j)
            case 0 %Inbound to helipad
                paxD(i+1,j)=paxD(i,j)+dt*speedInbound;
                if paxD(i+1,j)>=0
                    paxD(i+1,j)=0;
                    paxState(i+1,j)=1;
                    waitWatch(j)=waitWatch(j)+dt;
                end
            case 1 %Checking in
                paxD(i+1,j)=0;
                if waitWatch(j)<timeCheckin
                    waitWatch(j)=waitWatch(j)+dt;
                else
                    waitWatch(j)=0;
                    if any(flyState==-2)
                        paxState(i+1,j)=3;
                        waitWatch(j)=waitWatch(j)+dt;
                    else
                        paxState(i+1,j)=2;
                    end
                end
            case 2 %Waiting for vehicle to arrive
                paxD(i+1,j)=0;
                if any(flyState==-2)
                    paxState(i+1,j)=3;
                    waitWatch(j)=waitWatch(j)+dt;
                end
            case 3 %Boarding
                paxD(i+1,j)=0;
                if waitWatch(j)<timeBoard
                    waitWatch(j)=waitWatch(j)+dt;
                else
                    waitWatch(j)=0;
                    temp=paxState(i,:); temp(j)=[];
                    if all(temp>3)
                        paxState(i+1,j)=5;
                    else
                        paxState(i+1,j)=4;
                    end
                end
            case 4 %Waiting for passengers to load
                paxD(i+1,j)=0;
                if all(paxState(i,:)>3)
                    paxState(i+1,j)=5;
                end
            case 5 %Flying
                paxD(i+1,j)=paxD(i,j)+speedCruise*dt;
                if paxD(i+1,j)>=distance
                    paxD(i+1,j)=distance;
                    paxState(i+1,j)=6;
                    waitWatch(j)=waitWatch(j)+dt;
                end
            case 6 %Alighting
                paxD(i+1,j)=distance;
                if waitWatch(j)<timeAlight
                    waitWatch(j)=waitWatch(j)+dt;
                else
                    waitWatch(j)=0;
                    paxState(i+1,j)=7;
                end
            case 7 %Outbound to destination
                paxD(i+1,j)=paxD(i,j)+dt*speedOutbound;
                if paxD(i+1,j)>=distance+finish(j)
                    paxD(i+1,j)=distance+finish(j);
                    paxState(i+1,j)=8;
                end
            case 8 %Finished
                paxD(i+1,j)=paxD(i,j);
            otherwise
                warning('Unknown passenger state')
        end
    end
end

%% Distance between start and finish
x0=-start.*cos(startAngle);
y0=-start.*sin(startAngle);
x1=finish.*cos(finishAngle)+distance;
y1=finish.*sin(finishAngle);
distanceBetween=sqrt((x1-x0).^2+(y1-y0).^2);

totalTripTime=zeros(1,pax);
for j=1:pax
    finishIndex=find(paxState(:,j)==8,1,'first');
    if isempty(finishIndex)
        totalTripTime(j)=nan;
    else
        totalTripTime(j)=time(finishIndex);
    end
end
effectiveSpeed = distanceBetween./totalTripTime;

%% Plot results
if drawon
    figure(1); clf;
    for j=1:pax
        subplot(2,1,1); hold on
        plot(time/60,paxD(:,j))
        
        subplot(2,1,2); hold on
        plot(time/60,paxState(:,j))
        
        legendName{j}=['Passenger ' num2str(j)];
    end
    
    subplot(2,1,1);
    xlabel('Time [min]')
    ylabel('Position [m]')
    title(['Max Cruise Speed = ' num2str(speedCruiseMax) ...
        ' m/s. Trip distance = ' num2str(distance/1000) ' km.'])
    
    subplot(2,1,2); hold on
    xlabel('Time [min]')
    yticks(0:8)
    yticklabels({'Inbound','Checkin','Waiting for vehicle','Boarding',...
        'Waiting for passenger','Flying','Alighting','Outbound','Finished'})
    legend(legendName{:},'location','southeast')
end
